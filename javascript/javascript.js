// $(document).ready(function () {
//     btnSubmit.textContent = "SAVE";
//     btnSubmit.style.fontWeight = "BOLD";
// });
var presentID = 0;
var firstname_field = $("#firstname");
var lastname_field = $('#lastname');
var email_field = $('#email');
var mobilenumber_field = $('#mobilenumber');
var role_field = $('#role');
var department_field = $('#department');
var companyname_field = $('#companyname');
var reseachfrequency_field = $('#reseachfrequency');
var btnSubmit = $("#btn-submit");
btnSubmit.css("font-weight", "bold");
var dataArrayObject = [];
function createDetail(id, firstname, lastname, email, mobienumber, role, department, companyname, reseachfrequency) {
    this.id = id;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.mobienumber = mobienumber;
    this.role = role;
    this.department = department;
    this.companyname = companyname;
    this.reseachfrequency = reseachfrequency;
}
var detail1 = new createDetail("456852", "Vinh", "Nguyen", "vnguyen92@dxc.com", "0911901825", "Front End Developer", "", "DXC Vietnam", "VINH");
var detail2 = new createDetail("456789", "Trang", "Nguyen", "tnguyen1@dxc.com", "0911901825", "Front End Developer", "", "DXC Vietnam", "TRANG");
var detail3 = new createDetail("456123", "Huy", "Dong", "hdong5@dxc.com", "0911901825", "Front End Developer", "", "DXC Vietnam", "HUY");
var detail4 = new createDetail("456456", "Tai", "Le", "tle42@dxc.com", "0911901825", "BA", "", "DXC Vietnam", "TAI");
dataArrayObject.push(detail1, detail2, detail3, detail4);

var appendItem = (item, i) => {
    var outputHTML = "";
    outputHTML += "<tr>";
    outputHTML += "<td>" + [i] + "</td>"
        + "<td>" + item.firstname + " " + item.lastname + "</td>"
        + "<td>" + item.email + "</td>"
        + "<td>" + item.mobienumber + "</td>"
        + "<td>" + item.role + "</td>"
        + "<td>" + item.department + "</td>"
        + "<td>" + '<span data-id="' + (item.id) + '" class="glyphicon glyphicon-edit" >Edit</span>' + '<span data-id="' + (item.id) + '"class="glyphicon glyphicon-trash">Remove</span>' + "</td>"
        + "</tr>";
    $("#data").append(outputHTML);
}
var showAllData = () => {
    var outputHTML = "";
    for (var i = 0; i < dataArrayObject.length; i++) {
        const item = dataArrayObject[i] || {};
        appendItem(item, i + 1);
        // var id = dataArrayObject[count].id;
        // var firstname = dataArrayObject[count].firstname;
        // var lastname = dataArrayObject[count].lastname;
        // var email = dataArrayObject[count].email;
        // var mobienumber = dataArrayObject[count].mobienumber;
        // var role = dataArrayObject[count].role;
        // var department = dataArrayObject[count].department;
        // outputHTML += "<tr>";
        // outputHTML += "<td>" + [count + 1] + "</td>"
        //     + "<td>" + firstname + " " + lastname + "</td>"
        //     + "<td>" + email + "</td>"
        //     + "<td>" + mobienumber + "</td>"
        //     + "<td>" + role + "</td>"
        //     + "<td>" + department + "</td>"
        //     + "<td>" + '<span data-id="' + (id) + '" class="glyphicon glyphicon-edit" >Edit</span>' + '<span data-id="' + (id) + '"class="glyphicon glyphicon-trash">Remove</span>' + "</td>"
        //     + "</tr>"
    }
    // $("#data").append(outputHTML);
};
showAllData();
function checkValidationForm() {
    var checkmobilenumber = /[0-9]{3}[0-9]{3}[0-9]{4}/;
    var checkemail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var error_mesage = $(".error-message");
    function testValidationForm() {
        if (firstname_field.val() === "") {
            firstname_field.addClass("error-field");
            error_mesage[0].innerText = "Name is required";
        }
        else {
            firstname_field.removeClass("error-field");
            error_mesage[0].innerText = "";
        }
        if (email_field.val() === "") {
            email_field.addClass("error-field");
            error_mesage[1].innerText = "Email is required";
        }
        else {
            if (checkemail.test(email_field.val()) === false) {
                email_field.addClass("error-field");
                error_mesage[1].innerText = "Email is invalid";
            }
            else {
                email_field.removeClass("error-field");
                error_mesage[1].innerText = "";
            }
        }
        if (role_field.val() === "" || role_field.val() === null) {
            role_field.addClass("error-field");
            error_mesage[2].innerText = "Your role is required";
        }
        else {
            role_field.removeClass("error-field");
            error_mesage[2].innerText = "";
        }
        if (lastname_field.val() === "") {
            lastname_field.addClass("error-field");
            error_mesage[4].innerText = "Last name is required";
        }
        else {
            lastname_field.removeClass("error-field");
            error_mesage[4].innerText = "";
        }
        if (mobilenumber_field.val() === "") {
            mobilenumber_field.addClass("error-field");
            error_mesage[5].innerText = "Mobile number is required";
        }
        else {
            if (checkmobilenumber.test(mobilenumber_field.val()) === false) {
                error_mesage[5].innerText = "Mobile number must be 10 digit";
            }
            else {
                mobilenumber_field.removeClass("error-field");
                error_mesage[5].innerText = "";
            }
        }
        if (spanfirstname.textContent === "" && spanemail.textContent === "" && spanlastname.textContent === "" &&
            spanrole.textContent === "" && spanmobilenumber.textContent === "") {
            return true;
        }
        else {
            return false;
        }
    }
    if (btnSubmit.attr("data-id") === "save") {
        if (testValidationForm()) {
            var id = Math.round(Math.random() * 10000);
            var newDetail = new createDetail(id, firstname_field.val(), lastname_field.val(), email_field.val(), mobilenumber_field.val(), role_field.val(), department_field.val(), companyname_field.val(), reseachfrequency_field.val());
            dataArrayObject.push(newDetail);
            appendItem(newDetail, dataArrayObject.length);
            // showAllData(dataArrayObject.length - 1);
            var span = $('[ data-id = "' + id + '"] ');
            span[0].addEventListener("click", editTable, false);
            span[1].addEventListener("click", deleteRow, false);
            $("#form-table")[0].reset();
            return false;
        }
    }
    else {
        if (testValidationForm()) {

            // for (var i = 0; i < dataArrayObject.length; i++) {
            //     if (presentID == dataArrayObject[i].id) {
            //         dataArrayObject[i].firstname = firstname_field.val();
            //         dataArrayObject[i].lastname = lastname_field.val();
            //         dataArrayObject[i].email = email_field.val();
            //         dataArrayObject[i].mobienumber = mobilenumber_field.val();
            //         dataArrayObject[i].role = role_field.val();
            //         dataArrayObject[i].department = department_field.val();
            //         dataArrayObject[i].companyname = companyname_field.val();
            //         dataArrayObject[i].reseachfrequency = reseachfrequency_field.val();
            //         break;
            //     }
            // }
            var tbody = document.getElementById("data")
            var listOfRow = tbody.getElementsByTagName("tr");
            for (var i = 0; i < listOfRow.length; i++) {
                var listOfSpan = listOfRow[i].getElementsByTagName("span");
                if (listOfSpan[0].getAttribute("data-id") == presentID) {
                    var listofTData = listOfRow[i].getElementsByTagName("td");
                    listofTData[1].textContent = firstname_field.val() + " " + lastname_field.val();
                    listofTData[2].textContent = email_field.val();
                    listofTData[3].textContent = mobilenumber_field.val();
                    listofTData[4].textContent = role_field.val();
                    listofTData[5].textContent = department_field.val();
                }
            }
            btnSubmit.attr('data-id', 'save');
            btnSubmit.html('SAVE');
            // listOfRow[count - 1].style.background = "white";
            $("#form-table")[0].reset();
            $('#firstname').focus();
            return false;
        }
    }
}
function editTable() {
    btnSubmit.attr('data-id', 'update');
    btnSubmit.html('UPDATE');
    var tbody = document.getElementById("data")
    var listOfRow = tbody.getElementsByTagName("tr");
    for (var i = 0; i < dataArrayObject.length; i++) {
        var listOfSpan = listOfRow[i].getElementsByTagName("span");
        if (listOfSpan[0].getAttribute("data-id") == $(this).attr("data-id")) {
            listOfRow[i].style.background = "#FFFACD";
        }
        else {
            listOfRow[i].style.background = "white";
        }
        if ($(this).attr("data-id") == dataArrayObject[i].id) {
            firstname_field.val(dataArrayObject[i].firstname);
            lastname_field.val(dataArrayObject[i].lastname);
            email_field.val(dataArrayObject[i].email);
            mobilenumber_field.val(dataArrayObject[i].mobienumber);
            role_field.val(dataArrayObject[i].role);
            department_field.val(dataArrayObject[i].department);
            companyname_field.val(dataArrayObject[i].companyname);
            reseachfrequency_field.val(dataArrayObject[i].reseachfrequency);
            presentID = dataArrayObject[i].id;
        }
    }
}
function deleteRow() {
    var table = document.getElementsByClassName("table");
    var tbody = document.getElementById("data")
    var listOfRow = tbody.getElementsByTagName("tr");

    if (listOfRow.length === 1) {
        table[1].deleteRow(1);
    }
    else {
        for (var i = 1; i <= listOfRow.length; i++) {
            var listOfSpan = listOfRow[i - 1].getElementsByTagName("span");
            if (listOfSpan[0].getAttribute("data-id") == $(this).attr("data-id")) {
                table[1].deleteRow(i);
            }
            if ($(this).attr("data-id") == dataArrayObject[i - 1].id) {
                dataArrayObject.slice(i, 1);
                console.log(dataArrayObject);
            }
        }
    }
    cancelForm();
}
function cancelForm() {
    var error_mesage = $(".error-message");
    for (var i = 0; i < error_mesage.length; i++) {
        error_mesage[i].innerText = "";
    }
    $("#form-table")[0].reset();
    btnSubmit.attr('data-id', 'save');
    btnSubmit.html('SAVE');
    document.getElementById('firstname').focus();
    firstname_field.removeClass("error-field");;
    email_field.removeClass("error-field");;
    role_field.removeClass("error-field");;
    lastname_field.removeClass("error-field");;
    mobilenumber_field.removeClass("error-field");;
}
//b3 cau truc theo hang ngang
var classnameEdit = document.getElementsByClassName('glyphicon-edit');
for (var i = 0; i < classnameEdit.length; i++) {
    classnameEdit[i].addEventListener('click', editTable, false);
}
var classnameTrash = document.getElementsByClassName('glyphicon-trash');
for (var i = 0; i < classnameTrash.length; i++) {
    classnameTrash[i].addEventListener('click', deleteRow, false);
}